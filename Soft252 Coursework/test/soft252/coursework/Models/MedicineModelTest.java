/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package soft252.coursework.Models;

import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import soft252.coursework.Models.DoctorRelated.Medicine;
import soft252.coursework.Models.DoctorRelated.Prescription;

/**
 *
 * @author ivanc
 */
public class MedicineModelTest {
    
    public MedicineModelTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of alterMedicineStock method, of class MedicineModel.
     */
    @Test
    public void testAlterMedicineStock() throws Exception {
        System.out.println("alterMedicineStock");
        String name = "2344";
        int stock = 0;
        MedicineModel instance = MedicineModel.getInstance();
        instance.alterMedicineStock(name, stock);
    }

    /**
     * Test of addNewMedicine method, of class MedicineModel.
     */
    @Test
    public void testAddNewMedicine() {
        try {
            System.out.println("addNewMedicine");
            Medicine newMed = new Medicine("test", 0);
            MedicineModel instance = MedicineModel.getInstance();
            instance.addNewMedicine(newMed);
        } catch (Exception ex) {
            fail();
        }
    }

    @Test
    public void testAddNewMedicine2() {
        try {
            System.out.println("addNewMedicine");
            Medicine newMed = new Medicine("test", 0);
            MedicineModel instance = MedicineModel.getInstance();
            instance.addNewMedicine(newMed);
            fail();
        } catch (Exception ex) {
            
        }
    }
    
    /**
     * Test of searchMedicine method, of class MedicineModel.
     */
    @Test
    public void testSearchMedicine() {
        try {
            System.out.println("searchMedicine");
            String name = "test2";
            MedicineModel instance = MedicineModel.getInstance();
            Medicine med = new Medicine(name, 0);
            instance.addNewMedicine(med);
            Medicine expResult = med;
            Medicine result = instance.searchMedicine(name);
            assertEquals(expResult, result);
        } catch (Exception ex) {
            fail(ex.getMessage());
        }
    }

    
    /**
     * Test of createPrescription method, of class MedicineModel.
     */
    @Test
    public void testCreatePrescription() {
        System.out.println("createPrescription");
        String patient = "aa";
        String doctor = "bb";
        String note = "cc";
        MedicineModel instance = MedicineModel.getInstance();
        Prescription expResult = new Prescription(patient, doctor, note);
        Prescription result = instance.createPrescription(patient, doctor, note);
        assertEquals(expResult.getPatient(), result.getPatient());
    }
    
}
