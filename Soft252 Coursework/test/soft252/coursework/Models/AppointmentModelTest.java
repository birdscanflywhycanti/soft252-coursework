/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package soft252.coursework.Models;

import java.util.ArrayList;
import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import soft252.coursework.Models.DoctorRelated.Appointment;
import soft252.coursework.Models.DoctorRelated.AppointmentRequest;

/**
 *
 * @author ivanc
 */
public class AppointmentModelTest {
    
    public AppointmentModelTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getInstance method, of class AppointmentModel.
     */
    @Test
    public void testGetInstance() {
        System.out.println("getInstance");
        AppointmentModel expResult = null;
        AppointmentModel result = AppointmentModel.getInstance();
        assertEquals(true, result != null);

    }


    /**
     * Test of getAllAppointments method, of class AppointmentModel.
     */
    @Test
    public void testGetAllAppointments() {
        System.out.println("getAllAppointments");
        AppointmentModel instance = null;
        ArrayList<Appointment> expResult = null;
        ArrayList<Appointment> result = instance.getAllAppointments();
        assertEquals(expResult, result);
    }

    /**
     * Test of getFutureAppointments method, of class AppointmentModel.
     */
    @Test
    public void testGetFutureAppointments() {
        System.out.println("getFutureAppointments");
        String userID = "test";
        AppointmentModel instance = AppointmentModel.getInstance();
        ArrayList<Appointment> expResult = null;
        ArrayList<Appointment> result = instance.getFutureAppointments(userID);
        assertEquals(expResult, result);
    }

    /**
     * Test of checkAppointment method, of class AppointmentModel.
     */
    @Test
    public void testCheckAppointment() throws Exception {
        System.out.println("checkAppointment");
        ArrayList<Object> arrayList = null;
        String patientID = "";
        AppointmentModel instance = null;
        instance.checkAppointment(arrayList, patientID);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of createAppointmentBuilder method, of class AppointmentModel.
     */
    @Test
    public void testCreateAppointmentBuilder() {
        System.out.println("createAppointmentBuilder");
        DateTime apptTime = null;
        String doctorId = "";
        String patientId = "";
        AppointmentModel instance = null;
        Appointment.AppointmentBuilder expResult = null;
        Appointment.AppointmentBuilder result = instance.createAppointmentBuilder(apptTime, doctorId, patientId);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of addSpecialDuraton method, of class AppointmentModel.
     */
    @Test
    public void testAddSpecialDuraton() {
        System.out.println("addSpecialDuraton");
        Appointment.AppointmentBuilder builder = null;
        int specialDuration = 0;
        AppointmentModel instance = null;
        Appointment.AppointmentBuilder expResult = null;
        Appointment.AppointmentBuilder result = instance.addSpecialDuraton(builder, specialDuration);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of addNote method, of class AppointmentModel.
     */
    @Test
    public void testAddNote() {
        System.out.println("addNote");
        Appointment.AppointmentBuilder builder = null;
        String note = "";
        AppointmentModel instance = null;
        Appointment.AppointmentBuilder expResult = null;
        Appointment.AppointmentBuilder result = instance.addNote(builder, note);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of createAppointment method, of class AppointmentModel.
     */
    @Test
    public void testCreateAppointment() {
        System.out.println("createAppointment");
        DateTime apptTime = null;
        String doctorId = "";
        String patientId = "";
        int duration = 0;
        String note = "";
        AppointmentModel instance = null;
        instance.createAppointment(apptTime, doctorId, patientId, duration, note);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getAppointmentsRequests method, of class AppointmentModel.
     */
    @Test
    public void testGetAppointmentsRequests() {
        System.out.println("getAppointmentsRequests");
        AppointmentModel instance = null;
        ArrayList<AppointmentRequest> expResult = null;
        ArrayList<AppointmentRequest> result = instance.getAppointmentsRequests();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of createAppointmentRequest method, of class AppointmentModel.
     */
    @Test
    public void testCreateAppointmentRequest() {
        System.out.println("createAppointmentRequest");
        LocalDate start = null;
        LocalDate end = null;
        String doctorID = "";
        String PatientID = "";
        AppointmentModel instance = null;
        instance.createAppointmentRequest(start, end, doctorID, PatientID);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getAppointments method, of class AppointmentModel.
     */
    @Test
    public void testGetAppointments() {
        System.out.println("getAppointments");
        String id = "";
        AppointmentModel instance = null;
        ArrayList<Appointment> expResult = null;
        ArrayList<Appointment> result = instance.getAppointments(id);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }
    
}
