/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package soft252.coursework.Models;

import java.util.ArrayList;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import soft252.coursework.Models.Users.Admin;
import soft252.coursework.Models.Users.Doctor;
import soft252.coursework.Models.Users.Patient;
import soft252.coursework.Models.Users.Secetary;
import soft252.coursework.Models.Users.User;

/**
 *
 * @author ivanc
 */
public class UserModelTest {

    public UserModelTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of getAPPOINTMENT_MODEL method, of class UserModel.
     */
    @Test
    public void testGetAPPOINTMENT_MODEL() {
        System.out.println("getAPPOINTMENT_MODEL");
        UserModel instance = UserModel.getInstance();
        AppointmentModel expResult = AppointmentModel.getInstance();
        AppointmentModel result = instance.getAPPOINTMENT_MODEL();
        assertEquals(expResult, result);
    }

    /**
     * Test of getMEDICINE_MODEL method, of class UserModel.
     */
    @Test
    public void testGetMEDICINE_MODEL() {
        System.out.println("getMEDICINE_MODEL");
        UserModel instance = UserModel.getInstance();
        MedicineModel expResult = MedicineModel.getInstance();
        MedicineModel result = instance.getMEDICINE_MODEL();
        assertEquals(expResult, result);
    }

    /**
     * Test of getInstance method, of class UserModel.
     */
    @Test
    public void testGetInstance() {
        System.out.println("getInstance");
        UserModel expResult = UserModel.getInstance();
        UserModel result = UserModel.getInstance();
        assertEquals(expResult, result);
    }

    /**
     * Test of setInstance method, of class UserModel.
     */
    @Test
    public void testSetInstance() {
        System.out.println("setInstance");
        UserModel model = UserModel.getInstance();
        UserModel.setInstance(model);
    }

    /**
     * Test of addUser method, of class UserModel.
     */
    @Test
    public void testAddUser() {
        System.out.println("addUser");
        User user = new Admin("ada", "test", "testt");
        UserModel instance = UserModel.getInstance();
        instance.addUser(user);
    }

    /**
     * Test of getAllUsers method, of class UserModel.
     */
    @Test
    public void testGetAllUsers() {
        System.out.println("getAllUsers");
        UserModel instance = UserModel.getInstance();
        int expResult = 2;
        int result = instance.getAllUsers().size();
        assertEquals(expResult, result);
    }

    /**
     * Test of Login method, of class UserModel.
     */
    @Test
    public void testLogin() {
        System.out.println("Login");
        String userID = "D2";
        String password = "1234";
        UserModel instance = UserModel.getInstance();
        Doctor doc = new Doctor("1234", "D2", "address");
        instance.addUser(doc);
        User expResult = (User) doc;
        User result = instance.Login(userID, password);
        assertEquals(expResult, result);
    }

    @Test
    public void testLogin2() {
        System.out.println("Login2");
        String userID = "D2";
        String password = "12343";
        UserModel instance = UserModel.getInstance();
        User expResult = null;
        User result = instance.Login(userID, password);
        assertEquals(expResult, result);
    }

    /**
     * Test of getUser method, of class UserModel.
     */
    @Test
    public void testGetUser() {
        System.out.println("getUser");
        UserModel instance = UserModel.getInstance();
        Patient p = new Patient("1234", "bb", "fefe", "ddw", 0);
        String userID = p.getUserId();
        instance.addUser(p);
        User expResult = (User)p;
        User result = instance.getUser(userID);
        assertEquals(expResult, result);
    }

    /**
     * Test of addUnresolvedAccRemoval method, of class UserModel.
     */
    @Test
    public void testAddUnresolvedAccRemoval() {
        System.out.println("addUnresolvedAccRemoval");
        Patient user = new Patient("dd", "dd", "dd", "Male", 0);
        UserModel instance = UserModel.getInstance();
        instance.addUnresolvedAccRemoval(user);
    }

    /**
     * Test of removeAcc method, of class UserModel.
     */
    @Test
    public void testRemoveAcc() {
        System.out.println("removeAcc");
        Patient user = new Patient("ddd", "Ddd", "ddd", "ddd", 0);
        UserModel instance = UserModel.getInstance();
        instance.addUser(user);
        instance.removeAcc(user);
    }


    /**
     * Test of addAdminFeedBack method, of class UserModel.
     */
    @Test
    public void testAddAdminFeedBack() {
        System.out.println("addAdminFeedBack");
        String docID = "D2";
        String feedback = "bad";
        UserModel instance = UserModel.getInstance();
        instance.addAdminFeedBack(docID, feedback);
    }

    /**
     * Test of newUser method, of class UserModel.
     */
    @Test
    public void testNewUser_4args() {
        System.out.println("newUser");
        String type = "D";
        String pass = "1234";
        String name = "bob";
        String address = "adada";
        UserModel instance = UserModel.getInstance();
        instance.newUser(type, pass, name, address);
    }

    /**
     * Test of addRating method, of class UserModel.
     */
    @Test
    public void testAddRating() {
        System.out.println("addRating");
        String docID = "D2";
        String feedback = "123";
        int rating = 0;
        UserModel instance = UserModel.getInstance();
        instance.addRating(docID, feedback, rating);
    }

}
