/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import soft252.coursework.Models.DoctorRelated.Appointment;
import soft252.coursework.Models.DoctorRelated.AppointmentRequest;
import soft252.coursework.Models.DoctorRelated.Medicine;
import soft252.coursework.Models.DoctorRelated.MedicineEntry;
import soft252.coursework.Models.DoctorRelated.Prescription;
import soft252.coursework.Views.SecetaryView;
import soft252.coursework.Models.UserModel;
import soft252.coursework.Models.Users.Patient;
import soft252.coursework.Models.Users.Secetary;

public class SecetaryController {

    private SecetaryView theView;
    private Secetary currUser;
    private UserModel theModel = UserModel.getInstance();

    //sets up subscribers
    private void addViewListeners() {
        theView.addBtnRemoveListener(new DeleteListener());
        theView.addBtnReqRemoveListener(new DeleteReqListener());
        theView.addActPatientListener(new ActivateListener());
        theView.addNoActPatientRemoveListener(new NoActivateListener());
        theView.addCboAptListener(new AptCboListener());
        theView.addAptSubmitListener(new AptSubListener());
        theView.addCboPrescListener(new PrescListener());
        theView.addCboMedsListener(new MedsListener());
        theView.addBtnSearchListener(new SearchPatientListener());
        theView.addBtnGiveMedsListener(new MedGiveListener());
        theView.addSearchMedsListener(new SearchMedListener());
        theView.addRestockMedListener(new RestockListener());
    }

    //sets up view
    public void newView(Secetary _currUser) {
        currUser = _currUser;
        theView = new SecetaryView();
        viewInit();
        addViewListeners();
        theView.setVisible(true);
    }

    //sets up view data
    private void viewInit() {
        UpdateCbos();
        if (theModel.getAPPOINTMENT_MODEL().getAppointmentsRequests().size() >= 0) {
            updateAptTable(0);
        }

    }

    //refreshes combo boxes for account removals and requests
    private void UpdateCbos() {
        ArrayList<String> retNotActivated = new ArrayList<>();
        ArrayList<String> retActivated = new ArrayList<>();
        ArrayList<String> retReq = new ArrayList<>();
        ArrayList<String> retAppt = new ArrayList<>();
        DateTimeFormatter fmt = DateTimeFormat.forPattern("d MMMM yyyy, h a");
        //activation requests
        for (Patient patient : theModel.getPatients(false)) {
            retNotActivated.add(patient.getName() + " : " + patient.getUserId());
        }
        //removals
        for (Patient patient : theModel.getPatients(true)) {
            retActivated.add(patient.getName() + " : " + patient.getUserId());
        }
        //removal requests
        for (Patient patient : theModel.getUnresolvedAccRemovals()) {
            retReq.add(patient.getName() + " : " + patient.getUserId());
        }
        //appointment requests
        for (AppointmentRequest appointmentsRequest : theModel.getAPPOINTMENT_MODEL().getAppointmentsRequests()) {

            retAppt.add(appointmentsRequest.getPatientID());
        }

        theView.updateAllCbos(retActivated, retNotActivated, retReq, retAppt);
    }

    //loads new data into table when combo box is changed
    class AptCboListener implements ActionListener {

        public void actionPerformed(ActionEvent arg0) {
            updateAptTable(theView.getAptIndex());
        }
    }

    //loads new data into table based on the selected index of the combobox changed
    public void updateAptTable(int index) {
        AppointmentRequest appointmentRequest = theModel.getAPPOINTMENT_MODEL().getAppointmentsRequests().get(index);
        DateTimeFormatter fmt = DateTimeFormat.forPattern("d MMM yyyy");
        ArrayList<String> retArr = new ArrayList<>();
        retArr.add(appointmentRequest.getPatientID());
        retArr.add(theModel.getUser(appointmentRequest.getPatientID()).getName());
        retArr.add(appointmentRequest.getPreferredDoctor());
        retArr.add(theModel.getUser(appointmentRequest.getPreferredDoctor()).getName());
        retArr.add(appointmentRequest.getBeginDate().toString(fmt));
        retArr.add(appointmentRequest.getBeginDate().toString(fmt));
        theView.updateAptData(retArr);
    }

    //reject patient request
    class NoActivateListener implements ActionListener {

        public void actionPerformed(ActionEvent arg0) {
            try {
                theModel.removeAcc(theModel.getPatients(false).get(theView.getActiIndex()));
                UpdateCbos();
            } catch (Exception e) {
                theView.showMessage("An error occured", "Error", 0);
            }
        }
    }

    //accept patient request
    class ActivateListener implements ActionListener {

        public void actionPerformed(ActionEvent arg0) {
            try {
                theModel.getPatients(false).get(theView.getActiIndex()).setActivated(true);
                UpdateCbos();
            } catch (Exception e) {
                theView.showMessage("An error occured", "Error", 0);
            }
        }
    }

    //deletes selected patient from model
    class DeleteListener implements ActionListener {

        public void actionPerformed(ActionEvent arg0) {
            try {
                theModel.removeAcc(theModel.getPatients(true).get(theView.getDeleteIndex()));
                UpdateCbos();
            } catch (Exception e) {
                theView.showMessage("Unable to delete", "Error", 0);
            }
        }
    }

    //confirms request deletions
    class DeleteReqListener implements ActionListener {

        public void actionPerformed(ActionEvent arg0) {
            try {
                System.out.println(theView.getDeleteReqIndex());
                System.out.println(theModel.getUnresolvedAccRemovals().get(theView.getDeleteReqIndex()).getUserId());
                theModel.removeAcc(theModel.getUnresolvedAccRemovals().get(theView.getDeleteReqIndex()));
                UpdateCbos();
            } catch (Exception e) {
                theView.showMessage("Unable to delete", "Error", 0);
            }

        }
    }

    //creates an appointment based on user inputs for selected patient
    class AptSubListener implements ActionListener {

        public void actionPerformed(ActionEvent arg0) {
            try {
                if (theView.getAptIndex() == -1) {
                    throw new Exception("No requests pending");
                }
                theModel.getAPPOINTMENT_MODEL().checkAppointment(theView.getAptFormData(), currUser.getUserId());
                theModel.getAPPOINTMENT_MODEL().getAppointmentsRequests().remove(theView.getAptIndex());
                theView.removeListenersAptCbo();
                UpdateCbos();
                updateAptTable(0);
                theView.addCboAptListener(new AptCboListener());
            } catch (Exception e) {
                theView.showMessage(e.getMessage(), "Error", 0);
            }

        }
    }

    //
    
    //updates medicine tables to latest data
    private void updateMeds() {
        Patient patient = (Patient) theModel.getUser(theView.getPatientID());
        Prescription prescription = patient.getPrescriptions().get(theView.getPrescIndex());
        ArrayList<String> medArr = new ArrayList<>();
        for (MedicineEntry medicineEntry : prescription.getMedicineArray()) {
            medArr.add(medicineEntry.getName());
        }
        theView.removeMedListeners();
        theView.updateMedsCbo(medArr);
        theView.addCboMedsListener(new MedsListener());
    }

    //displays medicine data into labels when fired (stock and required amount)
    class MedsListener implements ActionListener {

        public void actionPerformed(ActionEvent arg0) {
            selectMed(theView.getMedsIndex());
        }
    }

    private void selectMed(int index) {
        Patient patient = (Patient) theModel.getUser(theView.getPatientID());
        MedicineEntry medicineEntry = patient.getPrescriptions().get(theView.getPrescIndex()).getMedicineArray().get(index);
        Medicine medicine = theModel.getMEDICINE_MODEL().searchMedicine(medicineEntry.getName());
        if (medicine != null) {
            theView.displayMedicineDetails(Integer.toString(medicine.getStock()), medicineEntry.getDosage());
        } else {
            theView.displayMedicineDetails("0", Integer.toString(medicineEntry.getQuantity()));
        }
    }

    //displays prescription data and medicines into combo box
    class PrescListener implements ActionListener {

        public void actionPerformed(ActionEvent arg0) {
            try {
                updateMeds();
                selectMed(0);
            } catch (Exception e) {
                theView.showMessage("Patient has no prescriptions", "Error", 0);
            }
        }
    }

    //gets all prescriptions for the selected user and adds to combobox
    class SearchPatientListener implements ActionListener {

        public void actionPerformed(ActionEvent arg0) {
            try {
                Patient patient = (Patient) theModel.getUser(theView.getPrescPatient());
                theView.setPatientID(patient.getUserId());

                DateTimeFormatter fmt = DateTimeFormat.forPattern("d MMMM yyyy, h:ma");
                ArrayList<String> prescArr = new ArrayList<>();
                
                if (patient.getPrescriptions().size() <= 0) {
                    throw new Exception("Patient has no prescriptions");
                }
                
                int i = 1;
                for (Prescription prescription : patient.getPrescriptions()) {
                    prescArr.add(i++ + ". " + prescription.getDateIssued().toString(fmt));
                }
                
                theView.setPatientID(patient.getUserId());
                theView.removePrescListeners();
                theView.updatePrescCbo(prescArr);
                updateMeds();
                selectMed(0);
                theView.addCboPrescListener(new PrescListener());

            } catch (Exception e) {
                theView.showMessage(e.getMessage(), "err", 0);//("Patient Could not be found", "Error", 0);
            }
        }
    }

    //collects patients medicine
    class MedGiveListener implements ActionListener {

        public void actionPerformed(ActionEvent arg0) {
            try {
                Patient patient = (Patient) theModel.getUser(theView.getPatientID());
                MedicineEntry medicineEntry = patient.getPrescriptions().get(theView.getPrescIndex()).getMedicineArray().get(theView.getMedsIndex());
                medicineEntry.setCollected(true);
                theModel.getMEDICINE_MODEL().alterMedicineStock(medicineEntry.getName(), 0 - medicineEntry.getQuantity());
            } catch (Exception e) {
                theView.showMessage(e.getMessage(), "Error", 0);
            }
        }

    }


    class SearchMedListener implements ActionListener {

        public void actionPerformed(ActionEvent arg0) {

            try {
                Medicine searchMed = theModel.getMEDICINE_MODEL().searchMedicine(theView.getMedName());
                theView.setMedString(searchMed.getMedicineName());
                theView.setStockString(Integer.toString(searchMed.getStock()));
            } catch (Exception e) {
                theView.setMedString(theView.getMedName());
                theView.showMessage("The searched medicine has never been in stock", "ALERT", 2);
            }
        }

    }

    //adds stock to medicine
    class RestockListener implements ActionListener {

        public void actionPerformed(ActionEvent arg0) {
            try {
                theModel.getMEDICINE_MODEL().alterMedicineStock(theView.getMedString(), Integer.parseInt(theView.getRestockAmount()));
                theView.setStockString(theView.getRestockAmount());
            } catch (Exception e) {
                theView.showMessage(e.toString(), "ERROR", 0);
            }
        }
    }
}
