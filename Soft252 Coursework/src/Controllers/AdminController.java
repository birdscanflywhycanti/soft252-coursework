/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JOptionPane;
import soft252.coursework.Models.UserModel;
import soft252.coursework.Models.Users.Admin;
import soft252.coursework.Models.Users.Doctor;
import soft252.coursework.Views.AdminDashboardView;

/**
 *
 * @author ivanc
 */
public class AdminController {
    //variables
    private AdminDashboardView theView;
    private UserModel theModel = UserModel.getInstance();
    private Admin currUser;

     //sets up subscribers for events
    private void addViewListeners() {
        theView.addFeedbackListener(new FeedbackListener());
        theView.addTypeChangedListener(new CboTypeChangeListener());
        theView.addRemoveAccListener(new RemoveAccListener());
        theView.addAccListener(new AddAccListener());
    }

    //generates and sets up view
    public void newView(Admin _currUser) {
        currUser = _currUser;
        System.out.println("HERE");
        theView = new AdminDashboardView();
        viewInit();
        addViewListeners();
        theView.setVisible(true);
    }

    //initialises view
    private void viewInit() {
        theView.setReviewTableValues(theModel.getDoctors());
        theView.setUserCboValues(theModel.getUserList("Secetary"), currUser.getUserId());
    }

    //atempts to add feedback from input data
    class FeedbackListener implements ActionListener {

        public void actionPerformed(ActionEvent arg0) {
            try {
                String feedback = theView.getFeedbackText();
                String docID = theView.getFeedbackDoctor();
                theModel.addAdminFeedBack(docID, feedback);
            } catch (Exception e) {
                //catches input errors
                System.err.println(e.getMessage());
                JOptionPane.showMessageDialog(theView, "Unable to find Doctor of " + theView.getFeedbackDoctor(), "Error", 0);
            }
        }
    }

    //updates combo box data when changed
    class CboTypeChangeListener implements ActionListener {

        public void actionPerformed(ActionEvent arg0) {
            theView.setUserCboValues(theModel.getUserList(theView.getComboUserType()), currUser.getUserId());
        }
    }

    //removes account selected when button is pressed
    class RemoveAccListener implements ActionListener {

        public void actionPerformed(ActionEvent arg0) {
            String userID = theView.getComboUserSelected();
            theModel.deleteUser(userID);
            theView.setUserCboValues(theModel.getUserList(theView.getComboUserType()), currUser.getUserId());
        }
    }

    //creates an account based on input data
    class AddAccListener implements ActionListener {

        public void actionPerformed(ActionEvent arg0) {
            try {
                String type = theView.getComboAccTypeSelected();
                String pass = theView.getNewUserPass();
                String name = theView.getName();
                String address = theView.getAddress();
                theModel.newUser(type, pass, name, address);
                theView.setUserCboValues(theModel.getUserList(theView.getComboAccTypeSelected()), currUser.getUserId());
            } catch (Exception e) {
                theView.showMessage("An error occured. Please make sure all fields are correctly filled.", "Error", 0);
            }

        }
    }

}
