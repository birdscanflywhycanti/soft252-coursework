package Controllers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import soft252.coursework.Views.LoginView;
import soft252.coursework.Models.UserModel;
import soft252.MainClass.Soft252Coursework;
import soft252.coursework.Models.Users.Admin;
import soft252.coursework.Models.Users.Doctor;
import soft252.coursework.Models.Users.Patient;
import soft252.coursework.Models.Users.Secetary;
import soft252.coursework.Models.Users.User;
import soft252.coursework.Views.AdminDashboardView;
import soft252.coursework.Views.RequestAccView;

public class LoginController {

    private LoginView theView;
    private UserModel theModel = UserModel.getInstance();

    //sets up subscribers
    private void addViewListeners() {
        this.theView.addLoginListener(new LoginListner());
        this.theView.addRequestListener(new RequestListener());
    }

    //initialises view
    public void newView() {
        theView = new LoginView();
        theView.setVisible(true);
        addViewListeners();
    }
    
 //directs user to the correct login screen
    class LoginListner implements ActionListener {
        public void actionPerformed(ActionEvent arg0) {
            String userID, password;
            //catches any input errors or login fails
            try {
                userID = theView.getUserID();
                password = theView.getPassword();
                System.out.println(password);
                User currUser = theModel.Login(userID, password);
                System.out.println("Logged in as " + currUser.getUserId());
               
                if (currUser != null) {
                    
                    if (currUser.getRole() == 'A') {
                        Soft252Coursework.adminController.newView((Admin) currUser);
                        theView.dispose();
                    } 
                    
                    else if (currUser.getRole() == 'P') {
                        Patient p = (Patient) currUser;
                        if (p.isActivated()) {
                            Soft252Coursework.patientController.newView(p);
                            theView.dispose();
                        } else {
                            theView.showMessage("Your account has not been activated", "Not activated", 0);
                        }
                    } 
                    
                    else if (currUser.getRole() == 'S') {
                        Soft252Coursework.secetaryController.newView((Secetary) currUser);
                        theView.dispose();
                    } 
                    
                    else if (currUser.getRole() == 'D') {
                        Soft252Coursework.doctorController.newView((Doctor) currUser);
                        theView.dispose();
                    }
                }
            } catch (Exception e) {
                System.err.println(e.getMessage());
                theView.showMessage("We were unable to log you in with your credentials. please try again", "Wrong credentials", 0);
            }
        }
    }

    //directs user to request page
    class RequestListener implements ActionListener {

        public void actionPerformed(ActionEvent arg0) {
            RequestController requestController = new RequestController();
            requestController.newView();
            theView.dispose();
        }
    }

}
