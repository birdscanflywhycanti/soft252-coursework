/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JOptionPane;
import soft252.MainClass.Soft252Coursework;
import soft252.coursework.Models.UserModel;
import soft252.coursework.Views.RequestAccView;

/**
 *
 * @author ivanc
 */
public class RequestController {

    RequestAccView theView;
    UserModel theModel = UserModel.getInstance();

    //set up subscribers
    private void addViewListeners() {
        theView.addBackListener(new BackListener());
        theView.addSubmitListener(new SubmitListener());
    }

    //set up view
    public void newView() {
        theView = new RequestAccView();
        theView.setVisible(true);
        addViewListeners();
    }

    //set back button functionality
    class BackListener implements ActionListener {

        public void actionPerformed(ActionEvent arg0) {
            Soft252Coursework.loginController.newView();
            theView.dispose();
        }
    }

    //creates a new patient and sets activated to false. uses information fetched fom view
    class SubmitListener implements ActionListener {

        public void actionPerformed(ActionEvent arg0) {
            try {
                String name = theView.getName();
                String address = theView.getAddress();
                String password = theView.getPassword();
                String sex = theView.getSex();
                int age = Integer.parseInt(theView.getAge());
                String newUserID = theModel.newUser("Patient",password, name, address, sex, age);
                theView.showMessage( "Your UserID is " + newUserID, "Account Registered", 1);
                Soft252Coursework.loginController.newView();
                theView.dispose();
            } catch (Exception e) {
                theView.showMessage( "Input value error", "Error", 0);
            }

        }
    }
}
