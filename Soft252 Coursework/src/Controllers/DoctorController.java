/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import soft252.coursework.Models.DoctorRelated.Appointment;
import soft252.coursework.Models.DoctorRelated.Medicine;
import soft252.coursework.Models.DoctorRelated.MedicineEntry;
import soft252.coursework.Models.DoctorRelated.Prescription;
import soft252.coursework.Models.UserModel;
import soft252.coursework.Models.Users.Doctor;
import soft252.coursework.Models.Users.Patient;
import soft252.coursework.Views.DoctorView;

/**
 *
 * @author ivanc
 */
public class DoctorController {

    private DoctorView theView;
    private UserModel theModel = UserModel.getInstance();
    private Doctor currUser;

    //sets up subscribers for events
    private void addViewListeners() {
        theView.addApptListener(new ApptSelectListner());
        theView.addBookNewListener(new ApptSubListener());
        theView.addNoteListener(new NoteListener());
        theView.addPatientSelectoristener(new PatientListener());
        theView.addNewMedicineListener(new NewMedicineListener());
        theView.addNewPrescriptionListener(new NewPrescriptionListener());
        theView.addPrescSubmitListener(new PrescSubmitListener());
        theView.addCreateListener(new CreateListener());
    }

    //sets up view and initilises data
    public void newView(Doctor currDoctor) {
        this.currUser = currDoctor;
        theView = new DoctorView();
        theView.setVisible(true);
        addViewListeners();
        initAptCbo();
        initPatientCbo();
        updateMeds();
        viewAppointment(0);
    }

    //updates data in view when combobox is changed
    public void viewAppointment(int index) {
        theView.updateApptData(theModel.getAPPOINTMENT_MODEL().getAppointments(currUser.getUserId()).get(index));
    }

    //creates an appointment bases on input data and stores it
    class ApptSubListener implements ActionListener {

        public void actionPerformed(ActionEvent arg0) {
            try {
                if (theView.getApptCboIndex() == -1) {
                    throw new Exception("No Appointment selected");
                }
                String patient = theModel.getAPPOINTMENT_MODEL().getAppointments(currUser.getUserId()).get(theView.getApptCboIndex()).getPatient();
                ArrayList<Object> formData = theView.getApptFormData();
                formData.add(0, currUser.getUserId());
                formData.add(false);
                theModel.getAPPOINTMENT_MODEL().checkAppointment(formData, patient);
                initAptCbo();
                viewAppointment(0);
                theView.clearTextBox();
            } catch (Exception e) {
                theView.showMessage(e.getMessage(), "Error", 0);
            }

        }
    }

    //initialises combobox with stored data
    private void initPatientCbo() {
        ArrayList<String> patients = new ArrayList<>();
        for (Patient patient : theModel.getPatients(true)) {
            patients.add(patient.getUserId() + " : " + patient.getName());
        }
        theView.fillPatientCBO(patients);
    }

    //initialises combobox with stored data
    private void initAptCbo() {
        theView.removeCboListener();
        ArrayList<String> retArr = new ArrayList<>();
        DateTimeFormatter fmt = DateTimeFormat.forPattern("d MMMM yyyy, h:ma");
        for (Appointment appointment : theModel.getAPPOINTMENT_MODEL().getAppointments(currUser.getUserId())) {
            retArr.add(appointment.getStartTime().toString(fmt));
        }

        theView.fillAppointmentCBO(retArr);
        theView.addApptListener(new ApptSelectListner());
    }
    
    //inserts data into table when called
    class ApptSelectListner implements ActionListener {

        public void actionPerformed(ActionEvent arg0) {
            viewAppointment(theView.getApptCboIndex());
        }
    }

    //fills patient history table whne called
    public void fillPatienthistory() {
        String selecteduser = theModel.getPatients(true).get(theView.getPatientCboIndex()).getUserId();
        ArrayList<Appointment> appointments = new ArrayList<>();
        for (Appointment appointment : theModel.getAPPOINTMENT_MODEL().getAppointments(selecteduser)) {
            if (appointment.getStartTime().isBeforeNow()) {
                appointments.add(appointment);
            }
        }
        theView.fillHistory(appointments);
    }

    class PatientListener implements ActionListener {

        public void actionPerformed(ActionEvent arg0) {
            fillPatienthistory();
        }
    }

    //saves new note and refreshes table
    class NoteListener implements ActionListener {

        public void actionPerformed(ActionEvent arg0) {
            theModel.getAPPOINTMENT_MODEL()
                    .getAppointments(currUser.getUserId())
                    .get(theView.getApptCboIndex())
                    .setNote(theView.getNewNote());
            viewAppointment(theView.getApptCboIndex());
        }
    }

    //clears textboxes
    class NewPrescriptionListener implements ActionListener {

        public void actionPerformed(ActionEvent arg0) {
            theView.clearPresc();
        }
    }

    //adds new medicine into prescription by addint to the table
    class NewMedicineListener implements ActionListener {

        public void actionPerformed(ActionEvent arg0) {
            try {
                ArrayList<Object> readIn = theView.getTableData();
                String inputName = theView.getNewMedicine();
                String inputDose = theView.getDosage();
                String inputQuant = theView.getQuantity();
                for (String string : (ArrayList<String>) readIn.get(1)) {
                    if (string.equalsIgnoreCase(inputName)) {
                        throw new Exception("Medicine already added");
                    }
                }
                theView.addMedicineTb(inputName, inputQuant, inputDose);
            } catch (Exception e) {
                theView.showMessage(e.getMessage(), "Error", 0);
            }

        }
    }

    //gets data from the table and creats a prescription
    //stores prescription into searched patient
    class PrescSubmitListener implements ActionListener {

        public void actionPerformed(ActionEvent arg0) {
            try {
                Prescription prescription = new Prescription(theView.getPatientID(), currUser.getUserId(), theView.getPrescNote());
                ArrayList<Object> readIn = theView.getTableData();
                ArrayList<String> name = (ArrayList<String>) readIn.get(1);
                ArrayList<String> quant = (ArrayList<String>) readIn.get(2);
                ArrayList<String> dose = (ArrayList<String>) readIn.get(3);

                for (int i = 0; i < name.size(); i++) {
                    prescription.addMedicineEntry(new MedicineEntry(name.get(i), Integer.parseInt(quant.get(i)), dose.get(i)));
                }
                ((Patient) theModel.getUser(theView.getPatientID())).addPrescription(prescription);
            } catch (Exception e) {
                theView.showMessage(e.getMessage(), "Error", 0);
            }
        }
    }

    //refreshes medicine table
    public void updateMeds(){
        ArrayList<String> retArrayList = new ArrayList<>();
        for (Medicine med : theModel.getMEDICINE_MODEL().getAllMedicine()) {
            retArrayList.add(med.getMedicineName());
        }
        theView.fillMeds(retArrayList);
    }
    
    //checks if medicine already exists before creating a new entry
    class CreateListener implements ActionListener {

        public void actionPerformed(ActionEvent arg0) {
            try {
                String med = theView.getCreate();
                if(theModel.getMEDICINE_MODEL().searchMedicine(med) == null){
                    theModel.getMEDICINE_MODEL().addNewMedicine(new Medicine(med, 0));
                }else{
                    throw new Exception("Already exists");
                }
            } catch (Exception e) {
                theView.showMessage(e.getMessage(), "", 0);
            }
        }
    }
}
