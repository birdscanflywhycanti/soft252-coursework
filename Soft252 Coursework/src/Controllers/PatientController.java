/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import soft252.coursework.Models.DoctorRelated.Appointment;
import soft252.coursework.Models.DoctorRelated.MedicineEntry;
import soft252.coursework.Models.DoctorRelated.Prescription;
import soft252.coursework.Models.UserModel;
import soft252.coursework.Models.Users.Doctor;
import soft252.coursework.Models.Users.Patient;
import soft252.coursework.Views.PatientView;

/**
 *
 * @author ivanc
 */
public class PatientController {

    private PatientView theView;
    private UserModel theModel = UserModel.getInstance();
    private Patient currUser;

    //sets up subscribers
    private void addViewListeners() {
        theView.addFeedbackListener(new AptSubmitListener());
        theView.addAptSubmitListener(new AptSubmitListener());
        theView.addPrescListener(new PrescChangedListener());
        theView.addApptListener(new ApptListener());
    }

    //sets up view
    public void newView(Patient _currUser) {
        currUser = _currUser;
        theView = new PatientView();
        viewInit();
        addViewListeners();
        theView.setVisible(true);
    }

    //sets up data in view
    private void viewInit() {
        theView.setReviewTableValues(theModel.getDoctors());
        initHistoryTables();
        initPrescCbo();
        theView.setDefaultDates();
        initAppts();
    }

    //sets up appointment table and loads data in
    private void initAppts() {
        ArrayList<Appointment> appts = theModel.getAPPOINTMENT_MODEL().getFutureAppointments(currUser.getUserId());
        ArrayList<String> retArr = new ArrayList<>();
        DateTimeFormatter fmt = DateTimeFormat.forPattern("d MMMM yyyy, ha");
        for (Appointment appt : appts) {
            retArr.add(appt.getStartTime().toString(fmt));
        }
        if (appts.size() > 0) {
            updateApptTbl(0);
        }
        theView.initApptCbo(retArr);
    }

    //loads data into historty table
    private void initHistoryTables() {
        ArrayList<String> name = new ArrayList<>();
        ArrayList<String> date = new ArrayList<>();
        ArrayList<String> note = new ArrayList<>();
        ArrayList<Appointment> apps = theModel.getAPPOINTMENT_MODEL().getAppointments(currUser.getUserId());
        for (Appointment app : apps) {
            Doctor doc = (Doctor) theModel.getUser(app.getDoctor());
            name.add(doc.getName());
            DateTimeFormatter fmt = DateTimeFormat.forPattern("d MMMM yyyy, ha");
            date.add(app.getStartTime().toString(fmt));
            note.add(app.getNote());
        }
        theView.setHistoryTableValues(name, date, note);
    }

    //loads prescription headers into combo box
    private void initPrescCbo() {
        ArrayList<String> returnArr = new ArrayList<>();
        ArrayList<Prescription> pArr = currUser.getPrescriptions();
        int i = 1;
        for (Prescription prescription : pArr) {
            System.out.println(prescription.getNote());
            DateTimeFormatter fmt = DateTimeFormat.forPattern("d MMMM yyyy, hh:mma");
            returnArr.add(Integer.toString(i) + ". " + prescription.getDateIssued().toString(fmt));
            i++;
        }
        if (pArr.size() > 0) {
            setPerscTables(0);
        }
        theView.initPrescCbo(returnArr);
    }

    //requests an account removal
    class DeleteListener implements ActionListener {
        public void actionPerformed(ActionEvent arg0) {
            theModel.addUnresolvedAccRemoval(currUser);
        }

    }

    //updates appointment table when new one is selected
    class ApptListener implements ActionListener {

        public void actionPerformed(ActionEvent arg0) {
            updateApptTbl(theView.getApptIndex());
        }
    }

    //updates appointment table based on selected appointent
    private void updateApptTbl(int index) {
        ArrayList<Appointment> appts = theModel.getAPPOINTMENT_MODEL().getFutureAppointments(currUser.getUserId());
        theView.updateApptData(appts.get(index));
    }

    //displays dfferent prescriptions
    class PrescChangedListener implements ActionListener {

        public void actionPerformed(ActionEvent arg0) {
            System.out.println("CHANGED TO " + theView.getPresc());
            setPerscTables(theView.getPresc());
        }
    }

    //sets up prescription table for when a new one is loaded
    private void setPerscTables(int selected) {
        ArrayList<Prescription> pArr = currUser.getPrescriptions();
        Prescription thisPresc = pArr.get(selected);
        Doctor thisDoc = (Doctor) theModel.getUser(thisPresc.getDoctor());
        ArrayList<String> returnPatient = new ArrayList<>();
        ArrayList<String> returnDoctor = new ArrayList<>();
        ArrayList<MedicineEntry> returnMedicine = thisPresc.getMedicineArray();
        returnPatient.add(currUser.getName());
        returnPatient.add(currUser.getAddress());
        returnPatient.add(currUser.getSex());
        returnPatient.add(Integer.toString(currUser.getAge()));
        returnDoctor.add(thisDoc.getName());
        returnDoctor.add(thisDoc.getAddress());
        theView.setPrescTables(returnPatient, returnDoctor, returnMedicine, thisPresc.getNote());
    }

    //creates applcation to get an appointment. does error checking and validity checks
    class AptSubmitListener implements ActionListener {

        public void actionPerformed(ActionEvent arg0) {
            try {
                LocalDate start = theView.getStartDate();
                LocalDate end = theView.getEndDate();
                Doctor doc = (Doctor) theModel.getUser(theView.getPrefDoctor());
                if (end.isBefore(start)) {
                    throw new Exception("End date must be after start date");
                } else if (start.isBefore(new LocalDate().plusYears(5))) {
                    throw new Exception("You cannot book more than 5 years in advance.");
                } else {
                    theModel.getAPPOINTMENT_MODEL().createAppointmentRequest(start, end, doc.getUserId(), currUser.getUserId());
                }
                theView.resetRequest();
            } catch (Exception e) {
                System.out.println(e.getMessage());
                theView.showMessage("Bad Range or DoctorID", "Error", 0);
            }
        }
    }

    //sends stores and creates ratings 
    class FeedbackListener implements ActionListener {

        public void actionPerformed(ActionEvent arg0) {
            try {
                String docId = theView.getRateID();
                String feedback = theView.getRateFeedBack();
                int ratingNum = Integer.parseInt(theView.getRateRating());
                theModel.addRating(docId, feedback, ratingNum);
                theView.setReviewTableValues(theModel.getDoctors());
                theView.clearInputs();
            } catch (Exception e) {
                theView.showMessage("Please check all inputs are valid", "Unable to add rating", 0);
            }
        }
    }
}
