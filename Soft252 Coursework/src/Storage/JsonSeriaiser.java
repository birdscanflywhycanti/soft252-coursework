/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Storage;

import com.google.gson.Gson;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import soft252.coursework.Models.AppointmentModel;
import soft252.coursework.Models.MedicineModel;
import soft252.coursework.Models.UserModel;

/**
 *
 * @author ivanc
 */
public class JsonSeriaiser {

    public static UserModel getUsers() {
        Gson gson = new Gson();
        String content = "";
        try {
            content = new String(Files.readAllBytes(new File("." + File.separator + "user.json").toPath()));
        } catch (IOException e) {
            System.out.println(e.getMessage());
           // e.printStackTrace();
        }
        return gson.fromJson(content, UserModel.class);
    }

    public static AppointmentModel getAppointments() {
        Gson gson = new Gson();
        String content = "";
        try {
            content = new String(Files.readAllBytes(new File("." + File.separator + "appoitments.json").toPath()));
        } catch (IOException e) {
            System.out.println(e.getMessage());
            //e.printStackTrace();
        }
        return gson.fromJson(content, AppointmentModel.class);
    }

    public static MedicineModel getMedicine() {
        Gson gson = new Gson();
        String content = "";
        try {
            content = new String(Files.readAllBytes(new File("." + File.separator + "medicine.json").toPath()));
        } catch (IOException e) {
            System.out.println(e.getMessage());
            //e.printStackTrace();
        }
        return gson.fromJson(content, MedicineModel.class);
    }

    public static void shutdown(String path) throws IOException {
        Gson gson = new Gson();
        String user = gson.toJson(UserModel.getInstance());
        String med = gson.toJson(MedicineModel.getInstance());
        String appt = gson.toJson(AppointmentModel.getInstance());
        FileWriter fr1 = new FileWriter(new File("." + File.separator + "appoitments.json"), false);
        FileWriter fr2 = new FileWriter(new File("." + File.separator + "medicine.json"), false);
        FileWriter fr3 = new FileWriter(new File("." + File.separator + "user.json"), false);
        System.out.println(new File("." + File.separator + "user.json").toPath().toString());

        fr1.write(appt);
        fr2.write(med);
        fr3.write(user);
    }

}
