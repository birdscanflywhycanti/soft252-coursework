/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package soft252.MainClass;

import Controllers.AdminController;
import Controllers.DoctorController;
import Controllers.LoginController;
import Controllers.PatientController;
import Controllers.RequestController;
import Controllers.SecetaryController;
import Storage.JsonSeriaiser;
import java.awt.event.ActionEvent;
import java.io.File;
import soft252.coursework.Models.DoctorRelated.Appointment;
import soft252.coursework.Models.AppointmentModel;
import java.util.Date;
import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import soft252.coursework.Models.DoctorRelated.MedicineEntry;
import soft252.coursework.Models.DoctorRelated.Prescription;
import soft252.coursework.Models.DoctorRelated.Rating;
import soft252.coursework.Models.MedicineModel;
import soft252.coursework.Models.UserModel;
import soft252.coursework.Models.Users.Admin;
import soft252.coursework.Models.Users.Doctor;
import soft252.coursework.Models.Users.Patient;
import soft252.coursework.Models.Users.Secetary;
import soft252.coursework.Models.Users.User;
import soft252.coursework.Views.AdminDashboardView;
import soft252.coursework.Views.LoginView;

/**
 *
 * @author ivanc
 */
public class Soft252Coursework {

    public static User currentUser;
    public static LoginController loginController;
    public static AdminController adminController;
    public static RequestController requestController;
    public static PatientController patientController;
    public static SecetaryController secetaryController;
    public static DoctorController doctorController;

    /**
     * @param args the command line arguments
     */
    private static void populateClasses() {
        for (int i = 0; i < 10; i++) {
            Admin newAdmin = new Admin("1234", "Name " + i, "Address " + i);
            Doctor newDoctor = new Doctor("1234", "DoctorName" + i, "Address " + i);
            Secetary newSecetary = new Secetary("1234", "Name " + i, "Address " + i);
            Patient newPatient = new Patient("1234", "PatientName" + i, i + " Some street", "Male", i + 30);
            UserModel.getInstance().addUser(newAdmin);
            UserModel.getInstance().addUser(newDoctor);
            UserModel.getInstance().addUser(newSecetary);
            UserModel.getInstance().addUser(newPatient);
            newDoctor.addRating(new Rating(i % 5, "UserFeedbackHEREE"));
            newDoctor.addRating(new Rating(i, "LONG USER FEEDBACK!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"));
            if (i % 2 == 0) {
                newPatient.setActivated(true);
            }
            Prescription newPrescription2 = new Prescription(newPatient.getUserId(), newDoctor.getUserId(), "EMPTY NOTE1312");
            Prescription newPrescription = new Prescription(newPatient.getUserId(), newDoctor.getUserId(), "EMPTY NOTE");
            newPrescription.addMedicineEntry(new MedicineEntry("DrugName", 1 + i * 3, "SomeDosageText"));
            newPrescription2.addMedicineEntry(new MedicineEntry("DrugName2", i * 2 + 3, "SomeDosageText2"));
            newPatient.addPrescription(newPrescription);
            newPatient.addPrescription(newPrescription2);
            AppointmentModel.getInstance().createAppointment(new DateTime(2020, 3, 17 + i, (i % 5 + 12), (i % 2) * 30, 0, 0), newDoctor.getUserId(), newPatient.getUserId(), (i % 2 * 10), "test!!");
            AppointmentModel.getInstance().createAppointment(new DateTime(1120, 3, 17 + i, (i % 5 + 12), (i % 2) * 30, 0, 0), newDoctor.getUserId(), newPatient.getUserId(), (i % 2 * 10), "test old!!");
            AppointmentModel.getInstance().createAppointment(new DateTime(1123, 3, 17 + i, (i % 5 + 12), (i % 2) * 30, 0, 0), newDoctor.getUserId(), newPatient.getUserId(), (i % 2 * 10), "test old!!");
            AppointmentModel.getInstance().createAppointment(new DateTime(2022, 3, 17 + i, (i % 5 + 12), (i % 2) * 30, 0, 0), newDoctor.getUserId(), newPatient.getUserId(), (i % 2 * 10), "test!!");

            if (i % 3 == 0) {
                UserModel.getInstance().addUnresolvedAccRemoval(newPatient);
            }
            AppointmentModel.getInstance().createAppointmentRequest(new LocalDate(), new LocalDate(), "D2", newPatient.getUserId());
        }
    }

    public static void main(String[] args) {
        String file = Soft252Coursework.class.getProtectionDomain().getCodeSource().getLocation().getPath();
        //UserModel.getInstance();
        //UserModel.setInstance(JsonSeriaiser.getUsers());
        
        
        AppointmentModel.setInstance(JsonSeriaiser.getAppointments());
        MedicineModel.setInstance(JsonSeriaiser.getMedicine());
        
        //was unable to get the json to object working, but object to json works
        Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {
            public void run() {
                try {
                    JsonSeriaiser.shutdown(file);
                } catch (Exception e) {
                    System.out.println(e.getMessage());
                }

            }
        }, "Shutdown-thread"));

        populateClasses();
        //writes data to files correctly
        
        //creates controllers and shows first view.
        adminController = new AdminController();
        loginController = new LoginController();
        patientController = new PatientController();
        secetaryController = new SecetaryController();
        doctorController = new DoctorController();
        loginController.newView();

    }
}
