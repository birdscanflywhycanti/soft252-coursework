/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package soft252.coursework.Models;

import soft252.coursework.Models.DoctorRelated.AppointmentRequest;
import java.util.ArrayList;
import java.util.Date;
import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import soft252.coursework.Models.DoctorRelated.Appointment;

/**
 *
 * @author ivanc
 */
public class AppointmentModel {

    //use of singleton pattern
    private static AppointmentModel SINGLE_INSTANCE = new AppointmentModel();

    private AppointmentModel() {
    }
    private int currentId = 0;
    private ArrayList<Appointment> allAppointments = new ArrayList<>();
    private ArrayList<AppointmentRequest> allAppointmentRequests = new ArrayList<>();

    public static AppointmentModel getInstance() {
        return SINGLE_INSTANCE;
    }

    public static void setInstance(AppointmentModel model) {
        AppointmentModel.SINGLE_INSTANCE = model;
    }
    
    public ArrayList<Appointment> getAllAppointments() {
        return allAppointments;
    }

    public ArrayList<Appointment> getFutureAppointments(String userID) {
        ArrayList<Appointment> appts = getAppointments(userID);
        DateTime today = new DateTime();
        
        ArrayList<Appointment> retArr = new ArrayList<>();
        for (Appointment appt : appts) {
            if (today.isBefore(appt.getStartTime())) {
                retArr.add(appt);
            }
        }
        return retArr;
    }

    public void checkAppointment(ArrayList<Object> arrayList, String patientID) throws Exception {
        String docID = arrayList.get(0).toString();
        int year = Integer.parseInt(arrayList.get(1).toString());
        int month = Integer.parseInt(arrayList.get(2).toString());
        int day = Integer.parseInt(arrayList.get(3).toString());
        int hour = Integer.parseInt(arrayList.get(4).toString());
        int min = Integer.parseInt(arrayList.get(5).toString());
        
        boolean longApp = (boolean) arrayList.get(6);
        DateTime dateTime = new DateTime(year, month, hour, min, 0, 0);
        
        int duration = 0;

        if ((hour < 8 || hour > 18)) {
            throw new Exception("Appointment out of hours");
        }
        if (dateTime.isBeforeNow()){
            throw new Exception("Appointments must be set for the future");
        }

        ArrayList<Appointment> docAppointments = getAppointments(docID);
        for (Appointment docAppointment : docAppointments) {
            if ((docAppointment.getStartTime().isAfter(dateTime) && docAppointment.getStartTime().plusMinutes(docAppointment
                    .getDuration()).isBefore(dateTime.plusMinutes((longApp == false) ? 30 : 60)))) {

                throw new Exception("Doctor is already booked at this time");
            }
            createAppointment(dateTime, docID.toUpperCase(), patientID, (longApp == true) ? 30 : 60, "");
        }
    }

    public Appointment.AppointmentBuilder createAppointmentBuilder(DateTime apptTime, String doctorId, String patientId) {
        currentId++;
        return new Appointment.AppointmentBuilder(currentId, doctorId, patientId)
                .start(apptTime);
    }

    public Appointment.AppointmentBuilder addSpecialDuraton(Appointment.AppointmentBuilder builder, int specialDuration) {
        return builder.duration(specialDuration);
    }

    public Appointment.AppointmentBuilder addNote(Appointment.AppointmentBuilder builder, String note) {
        return builder.note(note);
    }

    public void createAppointment(DateTime apptTime, String doctorId, String patientId, int duration, String note) {
        Appointment.AppointmentBuilder builder = createAppointmentBuilder(apptTime, doctorId, patientId);
        if (duration != 0) {
            addSpecialDuraton(builder, duration);
        }
        if (!note.equals("")) {
            addNote(builder, note);
        }
        allAppointments.add(builder.buildAppointment());
    }

    public ArrayList<AppointmentRequest> getAppointmentsRequests() {
        return allAppointmentRequests;
    }

    public void createAppointmentRequest(LocalDate start, LocalDate end, String doctorID, String PatientID) {
        AppointmentRequest newReq = new AppointmentRequest(start, end, PatientID, doctorID);
        allAppointmentRequests.add(newReq);
    }

    public ArrayList<Appointment> getAppointments(String id) {
        ArrayList<Appointment> returnArray = new ArrayList<>();
        for (Appointment appointment : allAppointments) {
            if (appointment.getDoctor().equalsIgnoreCase(id) || appointment.getPatient().equalsIgnoreCase(id)) {
                returnArray.add(appointment);
            }
        }
        return returnArray;
    }

}
