/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package soft252.coursework.Models.DoctorRelated;

/**
 *
 * @author ivanc
 */
public class Rating {
    int ratingNum;
    String feedback;
    
    public Rating(int ratingNum, String feedback) {
        this.ratingNum = ratingNum;
        this.feedback = feedback;
    }

    public int getRatingNum() {
        return ratingNum;
    }

    public void setRatingNum(int ratingNum) {
        this.ratingNum = ratingNum;
    }

    public String getFeedback() {
        return feedback;
    }

    public void setFeedback(String feedback) {
        this.feedback = feedback;
    }
    
}
