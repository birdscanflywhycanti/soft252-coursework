/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package soft252.coursework.Models.DoctorRelated;

/**
 *
 * @author ivanc
 */
public class Medicine {
    private String medicineName;
    private int stock;

    public Medicine(String medicineName, int stock) {
        this.medicineName = medicineName;
        this.stock = stock;
    }

    public String getMedicineName() {
        return medicineName;
    }

    public int getStock() {
        return stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }

    public void setMedicineName(String medicineName) {
        this.medicineName = medicineName;
    }


}
