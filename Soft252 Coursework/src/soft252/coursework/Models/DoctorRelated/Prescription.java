/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package soft252.coursework.Models.DoctorRelated;

import java.util.ArrayList;
import org.joda.time.DateTime;
import org.joda.time.LocalDateTime;
import soft252.coursework.Models.Users.Doctor;
import soft252.coursework.Models.Users.Patient;

/**
 *
 * @author ivanc
 */
public class Prescription {
    private final String patient;
    private final String doctor;
    private String note;
    private final DateTime dateIssued;
    private ArrayList<MedicineEntry> medicineArray = new ArrayList<>();

    public Prescription(String patient, String doctor, String note) {
        this.patient = patient;
        this.doctor = doctor;
        this.note = note;
        this.dateIssued = new DateTime();
    }

    public String getPatient() {
        return patient;
    }

    public String getDoctor() {
        return doctor;
    }

    public String getNote() {
        return note;
    }

    public ArrayList<MedicineEntry> getMedicineArray() {
        return medicineArray;
    }

    public void setNote(String note) {
        this.note = note;
    }
    
    public void addMedicineEntry(MedicineEntry entry){
        medicineArray.add(entry);
    }    

    public DateTime getDateIssued() {
        return dateIssued;
    }
    
}
