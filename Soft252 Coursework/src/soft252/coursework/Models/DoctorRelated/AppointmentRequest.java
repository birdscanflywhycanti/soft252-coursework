/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package soft252.coursework.Models.DoctorRelated;

import org.joda.time.DateTime;
import org.joda.time.LocalDate;

/**
 *
 * @author ivanc
 */
public class AppointmentRequest {

    private LocalDate beginDate;
    private LocalDate enddDate;
    private String patientID;
    private String preferredDoctor;

    public AppointmentRequest(LocalDate beginDate, LocalDate enddDate, String patientID, String preferredDoctor) {
        this.beginDate = beginDate;
        this.enddDate = enddDate;
        this.patientID = patientID;
        this.preferredDoctor = preferredDoctor;
    }

    public String getPatientID() {
        return patientID;
    }

    public void setPatientID(String patientID) {
        this.patientID = patientID;
    }

    public LocalDate  getBeginDate() {
        return beginDate;
    }

    public void setBeginDate(LocalDate  beginDate) {
        this.beginDate = beginDate;
    }

    public LocalDate  getEnddDate() {
        return enddDate;
    }

    public void setEnddDate(LocalDate  enddDate) {
        this.enddDate = enddDate;
    }

    public String getPreferredDoctor() {
        return preferredDoctor;
    }

    public void setPreferredDoctor(String preferredDoctor) {
        this.preferredDoctor = preferredDoctor;
    }
    
}
