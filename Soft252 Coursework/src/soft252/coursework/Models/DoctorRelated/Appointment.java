/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package soft252.coursework.Models.DoctorRelated;

import org.joda.time.DateTime;
import soft252.coursework.Models.Users.Doctor;
import soft252.coursework.Models.Users.Patient;

/**
 *
 * @author ivanc
 */
public class Appointment {
    private int appointmentId;
    private DateTime startTime;
    private int duration;
    private String note;
    private String doctor;
    private String patient;

    public Appointment(int appointmentId, DateTime startTime, int duration, String note, String doctor, String patient) {
        this.appointmentId = appointmentId;
        this.startTime = startTime;
        this.duration = duration;
        this.note = note;
        this.doctor = doctor;
        this.patient = patient;
    }

    public int getAppointmentId() {
        return appointmentId;
    }

    public void setAppointmentId(int appointmentId) {
        this.appointmentId = appointmentId;
    }

    public DateTime getStartTime() {
        return startTime;
    }

    public void setStartTime(DateTime startTime) {
        this.startTime = startTime;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getDoctor() {
        return doctor;
    }

    public void setDoctor(String doctor) {
        this.doctor = doctor;
    }

    public String getPatient() {
        return patient;
    }

    public void setPatient(String patient) {
        this.patient = patient;
    }

    public static class AppointmentBuilder {
        private int _appointmentId;
        private DateTime _startTime;
        private int _duration = 30;
        private String _note = "No note";
        private String _doctor;
        private String _patient;

        public AppointmentBuilder(int apptId, String doctorId, String patientID) {
            this._patient = patientID;
            this._doctor = doctorId;
            this._appointmentId = apptId;
        }
        public Appointment buildAppointment() {
            return new Appointment(_appointmentId, _startTime, _duration, _note, _doctor, _patient);
        }

        public AppointmentBuilder start(DateTime date) {
            this._startTime = date;
            return this;
        }

        public AppointmentBuilder duration(int duration){
            this._duration = duration;
            return this;
        }

        public AppointmentBuilder note(String note) {
            this._note = note;
            return this;
        }
    }
}
