/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package soft252.coursework.Models.Users;

import java.util.ArrayList;
import soft252.coursework.Models.DoctorRelated.Prescription;

/**
 *
 * @author ivanc
 */
public class Patient extends User {
    private Boolean isActivated = false;
    private String sex;
    private int age;
    private ArrayList<Prescription> prescriptions = new ArrayList<Prescription>();
    
    //constructor
    public Patient(String password, String _name, String _address, String sex, int age) {
        super(password, _name, _address);
        this.name = _name;
        this.address = _address;
        this.role = 'P';
        this.age = age;
        this.sex = sex;
    }

    //getters and setters
    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
    
    public void setActivated(Boolean bool){
        this.isActivated = bool;
    }

    public Boolean isActivated() {
        return isActivated;
    }

    public ArrayList<Prescription> getPrescriptions() {
        return prescriptions;
    }

    public void addPrescription(Prescription prescription) {
        prescriptions.add(prescription);
    }
    
    
    
    
}
