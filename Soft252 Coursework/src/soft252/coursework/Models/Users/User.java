/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package soft252.coursework.Models.Users;

/**
 *
 * @author ivanc
 */
public abstract class User {
    protected String password;
    protected char role;
    protected int id;
    protected String address;
    protected String name;
    protected static int currentId = 0;

    public final String getPassword() {
        return password;
    }

    public final void setPassword(String password) {
        this.password = password;
    }

    public final char getRole() {
        return role;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public final String getUserId() {
        return Character.toString(this.role) + Integer.toString(this.id);
    }

    public User(String password, String name, String address) {
        this.name = name;
        this.address = address;
        this.password = password;
        currentId++;
        this.id = currentId;
    }
}
