/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package soft252.coursework.Models.Users;

/**
 *
 * @author ivanc
 */
public class Admin extends User{
    public Admin(String password,String name, String address) {
        super(password, name, address);
        this.role = 'A';
    }
}
