/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package soft252.coursework.Models.Users;

import java.util.ArrayList;
import soft252.coursework.Models.DoctorRelated.Rating;

/**
 *
 * @author ivanc
 */
//constuctor
public class Doctor extends User{
    private ArrayList<Rating> allRatings = new ArrayList<Rating>();
    private ArrayList<String> adminFeedback = new ArrayList<String>();
    public Doctor(String password, String name, String address) {
        super(password, name, address);
        this.role = 'D';
    }

    //getters and setters
    public void addRating(Rating rating)    {
        this.allRatings.add(rating);
    }
    
    public ArrayList<Rating> getAllRatings(){
        return allRatings;
    }
    
    public ArrayList<String> getAdminFeedback(){
        return adminFeedback;
    }
    
    public void addAdminFeedback(String feedbackString)    {
        this.adminFeedback.add(feedbackString);
    }
    
}
