/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package soft252.coursework.Models.Users;

/**
 *
 * @author ivanc
 */
public class Secetary extends User {

    public Secetary(String password, String name, String address) {
        super(password, name, address);
        this.role = 'S';
    }

    public void activatePatientAcc(Patient patient) {
        patient.setActivated(true);
    }

    public void deactivatePatientAcc(Patient patient) {
        patient.setActivated(false);
    }

}
