/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package soft252.coursework.Models;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import soft252.coursework.Models.Users.User;
import java.util.ArrayList;
import java.util.Observer;
import soft252.MainClass.Soft252Coursework;
import soft252.coursework.Models.DoctorRelated.Rating;
import soft252.coursework.Models.Users.Admin;
import soft252.coursework.Models.Users.Doctor;
import soft252.coursework.Models.Users.Patient;
import soft252.coursework.Models.Users.Secetary;

/**
 *
 * @author ivanc
 */
public class UserModel {

    //uses singleton pattern
    private static UserModel SINGLE_INSTANCE = new UserModel();
    private ArrayList<User> allUsers = new ArrayList<>();
    private ArrayList<Patient> unresolvedAccRemovals = new ArrayList<Patient>();
    private static final AppointmentModel APPOINTMENT_MODEL = AppointmentModel.getInstance();
    private static final MedicineModel MEDICINE_MODEL = MedicineModel.getInstance();

    private UserModel() {
    }

    //getters and setters
    public AppointmentModel getAPPOINTMENT_MODEL() {
        return APPOINTMENT_MODEL;
    }

    public MedicineModel getMEDICINE_MODEL() {
        return MEDICINE_MODEL;
    }

    public static UserModel getInstance() {
        return SINGLE_INSTANCE;
    }

    public static void setInstance(UserModel model) {
        UserModel.SINGLE_INSTANCE = model;
    }

    public ArrayList<Patient> getUnresolvedAccRemovals() {
        return unresolvedAccRemovals;
    }

    public void addUser(User user) {
        allUsers.add(user);
    }

    public ArrayList<User> getAllUsers() {
        return allUsers;
    }

    //returns logged in user if cred correct
    public User Login(String userID, String password) {
        User thisUser = getUser(userID);
        if (thisUser.getPassword().equals(password)) {
            System.out.println(thisUser.getUserId());
            return thisUser;
        }

        return null;
    }

    //returns user from userid
    public User getUser(String userID) {
        for (User user : allUsers) {
            if (user.getUserId().equalsIgnoreCase(userID)) {
                return user;
            }
        }
        return null;
    }

    //returns all admins
    public ArrayList<Admin> getAdmins() {
        ArrayList<Admin> returnArray = new ArrayList<>();
        for (Object user : allUsers) {
            if (user instanceof Admin) {
                returnArray.add((Admin) user);
            }
        }
        return returnArray;
    }

    
    //returns all doctors
    public ArrayList<Doctor> getDoctors() {
        ArrayList<Doctor> returnArray = new ArrayList<>();
        for (Object user : allUsers) {
            if (user instanceof Doctor) {
                returnArray.add((Doctor) user);
            }
        }
        return returnArray;
    }

    //returns all secetarys
    public ArrayList<Secetary> getSecetarys() {
        ArrayList<Secetary> returnArray = new ArrayList<>();
        for (Object user : allUsers) {
            if (user instanceof Secetary) {
                returnArray.add((Secetary) user);
            }
        }
        return returnArray;
    }

    //adds user to array of unresolved removas
    public void addUnresolvedAccRemoval(Patient user) {
        unresolvedAccRemovals.add(user);
    }

    //deletes an account
    public void removeAcc(Patient user) {
        if (allUsers.contains((User) user)) {
            allUsers.remove(user);
        }
        if (unresolvedAccRemovals.contains(user)) {
            unresolvedAccRemovals.remove(user);
        }
    }

    //retrns all patients. (parameter determines if it returns active or non active accounts)
    public ArrayList<Patient> getPatients(boolean activatedParam) {
        ArrayList<Patient> returnArray = new ArrayList<>();
        for (Object user : allUsers) {
            if (user instanceof Patient) {
                if (((Patient) user).isActivated() == activatedParam) {
                    returnArray.add((Patient) user);
                }
            }
        }
        return returnArray;
    }

    //adds feedback to doctor from admin
    public void addAdminFeedBack(String docID, String feedback) {
        Doctor doc = (Doctor) getUser(docID);
        doc.addAdminFeedback(feedback);
    }

    //returns an array of users that belong to a certain subclass
    public ArrayList<String> getUserList(String type) {
        ArrayList<String> returnArray = new ArrayList<>();
        switch (type) {
            case "Admin":
                for (Admin user : getAdmins()) {
                    returnArray.add(user.getUserId());
                }
                break;
            case "Secetary":
                for (Secetary user : getSecetarys()) {
                    returnArray.add(user.getUserId());
                }
                break;
            case "Doctor":
                for (Doctor user : getDoctors()) {
                    returnArray.add(user.getUserId());
                }
                break;
            default:
                for (Patient user : getPatients(true)) {
                    returnArray.add(user.getUserId());
                }
                for (Patient user : getPatients(false)) {
                    returnArray.add(user.getUserId());
                }
                break;
        }
        return returnArray;
    }

    //fully removes a user from records
    public void deleteUser(String userID) {
        getAllUsers().remove(getUser(userID));
    }

    //creates a new user based on parameters and stores it. returns new userid
    public String newUser(String type, String pass, String name, String address) {
        String userID;
        switch (type) {
            case "Admin":
                Admin newAdmin = new Admin(pass, name, address);
                addUser(newAdmin);
                userID = newAdmin.getUserId();
                break;
            case "Secetary":
                Secetary newSecetary = new Secetary(pass, name, address);
                addUser(newSecetary);
                userID = newSecetary.getUserId();
                break;
            default:
                Doctor newDoctor = new Doctor(pass, name, address);
                addUser(newDoctor);
                userID = newDoctor.getUserId();
                break;
        }
        return userID;
    }

    //creates a patient
    public String newUser(String type, String pass, String name, String address, String sex, int age) {
        Patient newPatient = new Patient(pass, name, address, sex, age);
        addUser(newPatient);
        return newPatient.getUserId();

    }
    //adds a patient rating to a doctor
    public void addRating(String docID, String feedback, int rating) {
        Rating newRating = new Rating(rating, feedback);
        ((Doctor) getUser(docID)).addRating(newRating);
    }
}
