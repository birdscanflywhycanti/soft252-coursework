/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package soft252.coursework.Models;

import java.util.ArrayList;
import soft252.coursework.Models.DoctorRelated.Medicine;
import soft252.coursework.Models.DoctorRelated.Prescription;

/**
 *
 * @author ivanc
 */
public class MedicineModel {

    //singleton
    private static MedicineModel SINGLE_INSTANCE = new MedicineModel();
    private ArrayList<Medicine> allMedicine = new ArrayList<>();
    
    private MedicineModel() {
    }

    public static MedicineModel getInstance() {
        return SINGLE_INSTANCE;
    }
    
    public static void setInstance(MedicineModel model) {
        MedicineModel.SINGLE_INSTANCE = model;
    }
    public ArrayList<Medicine> getAllMedicine() {
        return allMedicine;
    }
    
    //manipulates item availability. creates new item if it doesnt already exist
    //else increase/decrease stock of existing entry
    public void alterMedicineStock(String name, int stock) throws Exception{
        boolean entryExists = false;
        for (Medicine med : allMedicine) {
            if (med.getMedicineName().equalsIgnoreCase(name)) {
                if (med.getStock() + stock < 0) {
                    throw new Exception("Out of stock");
                }
                med.setStock(med.getStock()+stock);
                entryExists = true;
                break;
            }
        }
        if (!entryExists) {
            addNewMedicine(new Medicine(name, stock));
        }
    }
    
    //creates new medicine and adds to storage
    public void addNewMedicine(Medicine newMed) throws Exception{
        boolean entryExists = false;
        for (Medicine med : allMedicine) {
            if (med.getMedicineName().equalsIgnoreCase(newMed.getMedicineName())){
                entryExists = true;
                break;
            }
        }
        if (entryExists) {
            throw new Exception ("Medicine already exists");
        }else{
            allMedicine.add(newMed);
        }
    }
    
    //returns a medicine object based on name
    public Medicine searchMedicine(String name){
        for (Medicine medicine : allMedicine) {
            if (name.equalsIgnoreCase(medicine.getMedicineName())){
                return medicine;
            }
        }
        return null;
    }
    
    //creates a new prescription
    public Prescription createPrescription(String patient, String doctor, String note) {
        return new Prescription(patient, doctor, note);
    }
    
    
    
}
