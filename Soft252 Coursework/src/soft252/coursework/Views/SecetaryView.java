/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package soft252.coursework.Views;

import java.awt.event.ActionListener;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import soft252.coursework.Models.Users.Patient;

/**
 *
 * @author ivanc
 */
public class SecetaryView extends javax.swing.JFrame {

    public SecetaryView() {
        initComponents();
    }

    public void addBtnRemoveListener(ActionListener listenForPrescChange) {
        btnRemPat.addActionListener(listenForPrescChange);
    }

    public void addCboAptListener(ActionListener listener) {
        cboAppReqs.addActionListener(listener);
    }

    public void addCboPrescListener(ActionListener listener) {
        cboPrescriptions.addActionListener(listener);
    }

    public void addBtnReqRemoveListener(ActionListener listenForPrescChange) {
        btnRemReq.addActionListener(listenForPrescChange);
    }

    public void addActPatientListener(ActionListener listenForPrescChange) {
        btnActivate.addActionListener(listenForPrescChange);
    }

    public void addAptSubmitListener(ActionListener listenForPrescChange) {
        btnSubmit.addActionListener(listenForPrescChange);
    }

    public void addNoActPatientRemoveListener(ActionListener listenForPrescChange) {
        btnNoActivate.addActionListener(listenForPrescChange);
    }

    public void addSearchMedsListener(ActionListener listenForPrescChange) {
        btnSearchMed.addActionListener(listenForPrescChange);
    }

    public void addRestockMedListener(ActionListener listenForPrescChange) {
        btnRestock.addActionListener(listenForPrescChange);
    }

    public void addBtnGiveMedsListener(ActionListener listener) {
        btnGiveMedicine.addActionListener(listener);
    }

    public void addBtnSearchListener(ActionListener listener) {
        btnPrescSearch.addActionListener(listener);
    }

    public void addCboMedsListener(ActionListener listener) {
        cboMedicines.addActionListener(listener);
    }

    public String getMedString() {
        return lblMedName.getText();
    }

    public void setMedString(String text) {
        lblMedName.setText(text);
    }

    
    public void setStockString(String text) {
        lblStockMed.setText(text);
    }
    
    public String getRestockAmount() {
        return txtAddMedAmount.getText();
    }

    public String getMedName() {
        return txtSearchMed.getText();
    }

    public int getDeleteIndex() {
        return cboRemPatients.getSelectedIndex();
    }

    public int getPrescIndex() {
        return cboPrescriptions.getSelectedIndex();
    }

    public int getMedsIndex() {
        return cboMedicines.getSelectedIndex();
    }

    public String getPatientID() {
        return lblPatientID.getText();
    }

    public void setPatientID(String string) {
        lblPatientID.setText(string);
    }

    public String getPrescPatient() {
        return txtPatientID.getText();
    }

    public ArrayList<Object> getAptFormData() {
        ArrayList<Object> retArr = new ArrayList<>();
        retArr.add(txtDoctor.getText());
        retArr.add(txtStartYear.getText());
        retArr.add(txtStartMonth.getText());
        retArr.add(txtStartDay.getText());
        retArr.add(txtHour.getText());
        retArr.add(cboMins.getSelectedItem().toString());
        retArr.add(togShort.getModel().isPressed());
        return retArr;
    }

    public void displayMedicineDetails(String stock, String quant) {
        lblStock.setText(stock);
        lblRequested.setText(quant);
    }

    public void updatePrescCbo(ArrayList<String> list) {
        cboPrescriptions.removeAllItems();
        for (String string : list) {
            cboPrescriptions.addItem(string);
        }
    }

    public void updateMedsCbo(ArrayList<String> list) {
        cboMedicines.removeAllItems();
        for (String string : list) {
            cboMedicines.addItem(string);
        }
    }

    public void removePrescListeners() {
        for (ActionListener actionListener : cboPrescriptions.getActionListeners()) {
            cboPrescriptions.removeActionListener(actionListener);
        }
    }

    public void removeSearachListeners() {
        for (ActionListener actionListener : btnPrescSearch.getActionListeners()) {
            btnPrescSearch.removeActionListener(actionListener);
        }
    }

    public void removeMedListeners() {
        for (ActionListener actionListener : cboMedicines.getActionListeners()) {
            cboMedicines.removeActionListener(actionListener);
        }
    }

    public void updateAptData(ArrayList<String> list) {
        DefaultTableModel tblModel = (DefaultTableModel) tblPatientDetails.getModel();
        tblModel.setNumRows(0);
        tblModel.addRow(new Object[]{"PatientID", list.get(0)});
        tblModel.addRow(new Object[]{"Patient Name", list.get(1)});
        tblModel.addRow(new Object[]{"DoctorID", list.get(2)});
        tblModel.addRow(new Object[]{"Doctor Name", list.get(3)});
        tblModel.addRow(new Object[]{"Start range", list.get(4)});
        tblModel.addRow(new Object[]{"End Range", list.get(5)});
    }

    public int getAptIndex() {
        return cboAppReqs.getSelectedIndex();
    }

    public int getDeleteReqIndex() {
        return cboRemPatientsReqs.getSelectedIndex();
    }

    public int getActiIndex() {
        return cboAccReqs.getSelectedIndex();
    }

    public void updateAllCbos(ArrayList<String> activated, ArrayList<String> notActivated, ArrayList<String> removeReqs, ArrayList<String> appts) {
        cboRemPatients.removeAllItems();
        cboAccReqs.removeAllItems();
        cboAppReqs.removeAllItems();
        cboRemPatientsReqs.removeAllItems();

        for (String patient : activated) {
            cboRemPatients.addItem(patient);
        }
        System.out.println(activated.size());
        for (String patient : removeReqs) {
            cboRemPatientsReqs.addItem(patient);
        }
        System.out.println(notActivated.size());
        for (String patient : notActivated) {
            cboAccReqs.addItem(patient);
        }
        System.out.println(appts.size());
        for (String patient : appts) {
            cboAppReqs.addItem(patient);
        }

    }

    public void removeListenersAptCbo() {
        for (ActionListener actionListener : cboAppReqs.getActionListeners()) {
            cboAppReqs.removeActionListener(actionListener);
        }
    }

    public void showMessage(String message, String title, int type) {
        JOptionPane.showMessageDialog(this, message, title, type);
    }

    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        mainTab = new javax.swing.JTabbedPane();
        jPanel1 = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        cboAccReqs = new javax.swing.JComboBox<>();
        btnNoActivate = new javax.swing.JButton();
        btnActivate = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        btnRemReq = new javax.swing.JButton();
        cboRemPatients = new javax.swing.JComboBox<>();
        jLabel1 = new javax.swing.JLabel();
        btnRemPat = new javax.swing.JButton();
        cboRemPatientsReqs = new javax.swing.JComboBox<>();
        jPanel2 = new javax.swing.JPanel();
        cboAppReqs = new javax.swing.JComboBox<>();
        txtStartYear = new javax.swing.JTextField();
        txtStartDay = new javax.swing.JTextField();
        txtStartMonth = new javax.swing.JTextField();
        lblDay = new javax.swing.JLabel();
        lblStart = new javax.swing.JLabel();
        lblYear = new javax.swing.JLabel();
        lblMonth = new javax.swing.JLabel();
        txtHour = new javax.swing.JTextField();
        lblStart1 = new javax.swing.JLabel();
        lblStart2 = new javax.swing.JLabel();
        jScrollPane3 = new javax.swing.JScrollPane();
        tblPatientDetails = new javax.swing.JTable();
        btnSubmit = new javax.swing.JButton();
        togShort = new javax.swing.JToggleButton();
        txtDoctor = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        cboMins = new javax.swing.JComboBox<>();
        PrescPanel = new javax.swing.JPanel();
        cboMedicines = new javax.swing.JComboBox<>();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        txtPatientID = new javax.swing.JTextField();
        btnGiveMedicine = new javax.swing.JButton();
        jLabel8 = new javax.swing.JLabel();
        btnPrescSearch = new javax.swing.JButton();
        lblStock = new javax.swing.JLabel();
        lblRequested = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        lblPatientID = new javax.swing.JLabel();
        cboPrescriptions = new javax.swing.JComboBox<>();
        jPanel3 = new javax.swing.JPanel();
        btnSearchMed = new javax.swing.JButton();
        jLabel11 = new javax.swing.JLabel();
        txtSearchMed = new javax.swing.JTextField();
        txtAddMedAmount = new javax.swing.JTextField();
        btnRestock = new javax.swing.JButton();
        jLabel12 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        lblMedName = new javax.swing.JLabel();
        lblStockMed = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jLabel3.setText("Patient Requests");

        btnNoActivate.setText("Reject");

        btnActivate.setText("Accept");

        jLabel2.setText("Remove Requests:");

        btnRemReq.setText("Remove");

        jLabel1.setText("Remove Patient: ");

        btnRemPat.setText("Remove");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(23, 23, 23)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel1)
                    .addComponent(jLabel2)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addGroup(jPanel1Layout.createSequentialGroup()
                            .addComponent(cboRemPatientsReqs, javax.swing.GroupLayout.PREFERRED_SIZE, 146, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGap(37, 37, 37)
                            .addComponent(btnRemReq, javax.swing.GroupLayout.PREFERRED_SIZE, 161, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(jPanel1Layout.createSequentialGroup()
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                .addComponent(cboAccReqs, 0, 146, Short.MAX_VALUE)
                                .addComponent(cboRemPatients, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addGap(37, 37, 37)
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(btnRemPat, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 161, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                    .addComponent(btnActivate)
                                    .addGap(18, 18, 18)
                                    .addComponent(btnNoActivate)))))
                    .addComponent(jLabel3))
                .addContainerGap(49, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap(36, Short.MAX_VALUE)
                .addComponent(jLabel3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cboAccReqs, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnNoActivate)
                    .addComponent(btnActivate))
                .addGap(18, 18, 18)
                .addComponent(jLabel1)
                .addGap(1, 1, 1)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cboRemPatients, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnRemPat))
                .addGap(18, 18, 18)
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cboRemPatientsReqs, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnRemReq))
                .addGap(37, 37, 37))
        );

        mainTab.addTab("Account manager", jPanel1);

        lblDay.setText("Day");

        lblStart.setText("Date");

        lblYear.setText("Year");

        lblMonth.setText("Month");

        lblStart1.setText("Hour (24h)");

        lblStart2.setText("Min");

        tblPatientDetails.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Header", "Value"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.Object.class
            };
            boolean[] canEdit = new boolean [] {
                false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tblPatientDetails.getTableHeader().setReorderingAllowed(false);
        tblPatientDetails.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tblPatientDetailsMouseClicked(evt);
            }
        });
        jScrollPane3.setViewportView(tblPatientDetails);

        btnSubmit.setText("Submit");

        togShort.setText("Long Apt (toggle)");

        jLabel4.setText("Doctor:");

        cboMins.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "00", "30" }));

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addGroup(jPanel2Layout.createSequentialGroup()
                                    .addComponent(lblStart1)
                                    .addGap(18, 18, 18)
                                    .addComponent(txtHour, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(lblStart2)
                                    .addGap(18, 18, 18)
                                    .addComponent(cboMins, javax.swing.GroupLayout.PREFERRED_SIZE, 53, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGroup(jPanel2Layout.createSequentialGroup()
                                    .addComponent(lblStart)
                                    .addGap(18, 18, 18)
                                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(txtStartDay, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(lblDay))
                                    .addGap(12, 12, 12)
                                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(txtStartMonth, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(lblMonth))
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(lblYear)
                                        .addComponent(txtStartYear, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE))))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                                .addComponent(btnSubmit)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(togShort, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                                .addComponent(jLabel4)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtDoctor, javax.swing.GroupLayout.PREFERRED_SIZE, 166, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addComponent(cboAppReqs, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(cboAppReqs, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtDoctor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel4))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lblDay)
                            .addComponent(lblYear)
                            .addComponent(lblMonth))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lblStart)
                            .addComponent(txtStartMonth, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtStartDay, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtStartYear, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lblStart1)
                            .addComponent(txtHour, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lblStart2)
                            .addComponent(cboMins, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(31, 31, 31)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(togShort)
                            .addComponent(btnSubmit)))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(12, 12, 12)
                        .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 206, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(22, Short.MAX_VALUE))
        );

        mainTab.addTab("Apointment Manager", jPanel2);

        jLabel5.setText("Patient ID:");

        jLabel6.setText("Amount requested:");

        jLabel7.setText("Stock:");

        btnGiveMedicine.setText("Give Medicine");

        jLabel8.setText("Prescriptions");

        btnPrescSearch.setText("Search");

        lblStock.setText("stock");

        lblRequested.setText("req");

        jLabel9.setText("Uncollected medicines:");

        jLabel10.setText("Patient:");

        lblPatientID.setText("Patient");

        cboPrescriptions.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        cboPrescriptions.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cboPrescriptionsActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout PrescPanelLayout = new javax.swing.GroupLayout(PrescPanel);
        PrescPanel.setLayout(PrescPanelLayout);
        PrescPanelLayout.setHorizontalGroup(
            PrescPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PrescPanelLayout.createSequentialGroup()
                .addGap(17, 17, 17)
                .addGroup(PrescPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel5)
                    .addComponent(btnGiveMedicine, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtPatientID)
                    .addComponent(btnPrescSearch, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 61, Short.MAX_VALUE)
                .addGroup(PrescPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(PrescPanelLayout.createSequentialGroup()
                        .addGroup(PrescPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel6)
                            .addComponent(jLabel7)
                            .addComponent(jLabel10))
                        .addGap(35, 35, 35)
                        .addGroup(PrescPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblStock)
                            .addComponent(lblRequested)
                            .addComponent(lblPatientID))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(PrescPanelLayout.createSequentialGroup()
                        .addGroup(PrescPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(PrescPanelLayout.createSequentialGroup()
                                .addGroup(PrescPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(cboMedicines, javax.swing.GroupLayout.PREFERRED_SIZE, 206, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, 177, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(cboPrescriptions, javax.swing.GroupLayout.PREFERRED_SIZE, 206, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(0, 18, Short.MAX_VALUE)))
                        .addContainerGap())))
        );
        PrescPanelLayout.setVerticalGroup(
            PrescPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PrescPanelLayout.createSequentialGroup()
                .addGap(32, 32, 32)
                .addGroup(PrescPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(jLabel8))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(PrescPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtPatientID, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cboPrescriptions, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(PrescPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnPrescSearch)
                    .addGroup(PrescPanelLayout.createSequentialGroup()
                        .addComponent(jLabel9)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(cboMedicines, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(PrescPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel10)
                    .addComponent(lblPatientID))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 12, Short.MAX_VALUE)
                .addGroup(PrescPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblStock)
                    .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 16, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(6, 6, 6)
                .addGroup(PrescPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnGiveMedicine)
                    .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 16, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblRequested))
                .addGap(32, 32, 32))
        );

        mainTab.addTab("Prescriptions", PrescPanel);

        btnSearchMed.setText("Search");

        jLabel11.setText("Search medicine");

        btnRestock.setText("Restock");

        jLabel12.setText("Amount");

        jLabel13.setText("Current Stock:");

        jLabel14.setText("Medicine:");

        lblMedName.setText("Paracetamol");

        lblStockMed.setText("0");

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(14, 14, 14)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtSearchMed)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel11)
                            .addComponent(btnSearchMed, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtAddMedAmount, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnRestock, javax.swing.GroupLayout.PREFERRED_SIZE, 165, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel12)
                            .addComponent(jLabel13)
                            .addComponent(jLabel14, javax.swing.GroupLayout.Alignment.TRAILING))
                        .addGap(30, 30, 30)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblMedName)
                            .addComponent(lblStockMed))
                        .addGap(211, 211, 211)))
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(8, 8, 8)
                .addComponent(jLabel11)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(txtSearchMed, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btnSearchMed)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 14, Short.MAX_VALUE)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel14)
                    .addComponent(lblMedName))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel13)
                    .addComponent(lblStockMed))
                .addGap(33, 33, 33)
                .addComponent(jLabel12)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtAddMedAmount, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnRestock))
                .addGap(23, 23, 23))
        );

        mainTab.addTab("Stock", jPanel3);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(mainTab)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(mainTab)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void tblPatientDetailsMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblPatientDetailsMouseClicked
        if (evt.getClickCount() == 2) {
            JTable target = (JTable) evt.getSource();
            int row = target.getSelectedRow();
            int column = target.getSelectedColumn();
            JOptionPane.showMessageDialog(this, tblPatientDetails.getModel().getValueAt(row, column).toString(), tblPatientDetails.getColumnName(column), 1);
        }
    }//GEN-LAST:event_tblPatientDetailsMouseClicked

    private void cboPrescriptionsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cboPrescriptionsActionPerformed

    }//GEN-LAST:event_cboPrescriptionsActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel PrescPanel;
    private javax.swing.JButton btnActivate;
    private javax.swing.JButton btnGiveMedicine;
    private javax.swing.JButton btnNoActivate;
    private javax.swing.JButton btnPrescSearch;
    private javax.swing.JButton btnRemPat;
    private javax.swing.JButton btnRemReq;
    private javax.swing.JButton btnRestock;
    private javax.swing.JButton btnSearchMed;
    private javax.swing.JButton btnSubmit;
    private javax.swing.JComboBox<String> cboAccReqs;
    private javax.swing.JComboBox<String> cboAppReqs;
    private javax.swing.JComboBox<String> cboMedicines;
    private javax.swing.JComboBox<String> cboMins;
    private javax.swing.JComboBox<String> cboPrescriptions;
    private javax.swing.JComboBox<String> cboRemPatients;
    private javax.swing.JComboBox<String> cboRemPatientsReqs;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JLabel lblDay;
    private javax.swing.JLabel lblMedName;
    private javax.swing.JLabel lblMonth;
    private javax.swing.JLabel lblPatientID;
    private javax.swing.JLabel lblRequested;
    private javax.swing.JLabel lblStart;
    private javax.swing.JLabel lblStart1;
    private javax.swing.JLabel lblStart2;
    private javax.swing.JLabel lblStock;
    private javax.swing.JLabel lblStockMed;
    private javax.swing.JLabel lblYear;
    private javax.swing.JTabbedPane mainTab;
    private javax.swing.JTable tblPatientDetails;
    private javax.swing.JToggleButton togShort;
    private javax.swing.JTextField txtAddMedAmount;
    private javax.swing.JTextField txtDoctor;
    private javax.swing.JTextField txtHour;
    private javax.swing.JTextField txtPatientID;
    private javax.swing.JTextField txtSearchMed;
    private javax.swing.JTextField txtStartDay;
    private javax.swing.JTextField txtStartMonth;
    private javax.swing.JTextField txtStartYear;
    // End of variables declaration//GEN-END:variables
}
